import React, { Component } from 'react';
import '../styles/style.sass';
import cd from '../image/welcome-icon/cd.png';
import headphone from '../image/welcome-icon/headphone.png';
import calendar from '../image/welcome-icon/calendar.png';
import seperator from '../image/line/line.png';

export class Musica extends Component {
    render(){
return(
    <div>
    <section className="welcome-to-musica">
      <div className="container">
        <h1 className="welcome">
          WELCOME TO <span>MUSICA,</span> CHECK OUR LATEST ALBUMS
          <img src={seperator} alt="Logo" className="line" />


        </h1>
        <div className="welcome-to-musica-content">
          <div className="options">
            
            <h3>  <img src={cd} alt="Logo" className="icon" />
 CHECK OUR CD COLLECTION</h3> 
             
            
            <p>
            Donec pede justo, fringilla vel, al, vulputate 
eget, arcu. In enim justo, lorem ipsum.{" "}
            </p>
          </div>
          <div className="options">
            <h3>
            <img src={headphone} alt="Logo" className="icon" />
                  LISTEN BEFORE PURCHASE
            </h3>
            <p>
            Donec pede justo, fringilla vel, al, vulputate 
eget, arcu. In enim justo, lorem ipsum.{" "}
            </p>
          </div>
          <div className="options">
          <h3>
          <img src={calendar} alt="Logo" className="icon" />
              UPCOMING EVENTS
</h3>
            <p>
            Donec pede justo, fringilla vel, al, vulputate 
eget, arcu. In enim justo, lorem ipsum.{" "}
            </p>
          </div>
        </div>
      </div>
    </section>
    

    
      </div> 
)

    }
}
